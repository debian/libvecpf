Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libvecpf
Source: https://github.com/Libvecpf/libvecpf

Files: *
Copyright: 2010-2015 International Business Machines Corporation and others.
License: LGPL-2.1

Files: m4/ax_cflags_warn_all.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2010 Rhys Ulerich <rhys.ulerich@gmail.com>
License: GPL-3+ with Autoconf exception

Files: m4/ax_append_flag.m4 m4/ax_check_compile_flag.m4
Copyright: 2008 Guido U. Draheim <guidod@gmx.de>
           2011 Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+ with Autoconf exception

Files: m4/ax_require_defined.m4
Copyright: 2014 Mike Frysinger <vapier@gentoo.org>
License: GNU-All-Permissive-License

Files: m4/ax_append_compile_flags.m4
Copyright: 2011 Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3+ with Autoconf exception

Files: config.sub config.guess
Copyright: 1992-2014 Free Software Foundation, Inc.
License: GPL-3+ with Autoconf exception

Files: man/libvecpf.3
Copyright: 1999 Andries Brouwer (aeb@cwi.nl)
           2011 IBM Corporation
License: GPL-2+

Files: debian/*
Copyright: 2016-2020 International Business Machines Corporation and others.
Author: Frédéric Bonnard <frediz@debian.org>
License: LGPL-2.1

License: GNU-All-Permissive-License
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

License: GPL-3+ with Autoconf exception
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 As a special exception, the respective Autoconf Macro's copyright owner
 gives unlimited permission to copy, distribute and modify the configure
 scripts that are the output of Autoconf when processing the Macro. You
 need not follow the terms of the GNU General Public License when using
 or distributing such scripts, even though portions of the text of the
 Macro appear in them. The GNU General Public License (GPL) does govern
 all other use of the material that constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute a
 modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.

License: GPL-2+
 This is free documentation; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License, or (at your option) any later version.
 .
 The GNU General Public License's references to "object code"
 and "executables" are to be interpreted as the output of any
 document formatting or typesetting system, including
 intermediate and printed output.
 .
 This manual is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

License: LGPL-2.1
 The Vector Printf Library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public License version
 2.1.
 .
 The Vector Printf Library is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 General Public License version 2.1 for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
